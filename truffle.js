/*
 * NB: since truffle-hdwallet-provider 0.0.5 you must wrap HDWallet providers in a 
 * function when declaring them. Failure to do so will cause commands to hang. ex:
 * ```
 * mainnet: {
 *     provider: function() { 
 *       return new HDWalletProvider(mnemonic, 'https://mainnet.infura.io/<infura-key>') 
 *     },
 *     network_id: '1',
 *     gas: 4500000,
 *     gasPrice: 10000000000,
 *   },
 */

//https://www.npmjs.com/package/dotenv
require('dotenv').config();

module.exports = {
    // See <http://truffleframework.com/docs/advanced/configuration>
    // to customize your Truffle configuration!
    networks: {
        development: {
            host: "localhost", // host gateway
            port: 8545,
            network_id: "*", // Match any network id
            gas: 6721975, // Gas limit used for deploys
            gasPrice: 1000000
        },
        ci: {
            host: "127.0.0.1", // host gateway
            port: 7545,
            network_id: "*" // Match any network id
        },
        rinkeby: {
            provider: function () {
                require('babel-register');
                const HDWalletProvider = require('truffle-hdwallet-provider');
                return new HDWalletProvider(
                    process.env.MNEMONIC,
                    'https://rinkeby.infura.io/v3/' + process.env.RINKEBY_INFURA_API_KEY
                )
            },
            network_id: 2,
            gas: 6700000, // Gas limit used for deploys
            gasPrice: 20000000000
        }
    },
    solc: {
        optimizer: {
            enabled: true,
            runs: 200
        }
    }
};
